package org.example.AbstractLayer;

import org.example.PageObject.RegistrationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AbstractLayer {

    WebDriver driver;
    public AbstractLayer(WebDriver driver) {
        this.driver=driver;
    }

    public void waitForElementToAppear(By findBy) {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOfElementLocated(findBy));

    }

    public void waitForWebElementToAppear(WebElement findBy) {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(findBy));

    }

    @FindBy(partialLinkText = "Signup")
    WebElement SignUpButton;

    public RegistrationPage goToRegistrationPage()
    {
        SignUpButton.click();
        RegistrationPage registrationPage = new RegistrationPage(driver);
        return registrationPage;
    }
    public void selectFromDropdown(WebElement dropdownElement, String optionValue) {
        // Initialize Select object with the dropdown WebElement
        Select dropdown = new Select(dropdownElement);

        // Select option by value
        dropdown.selectByValue(optionValue);
    }

    public void Click(WebElement element , WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
    }
}
