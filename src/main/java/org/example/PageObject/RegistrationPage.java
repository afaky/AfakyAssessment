package org.example.PageObject;

import com.github.dockerjava.core.dockerfile.DockerfileStatement;
import io.cucumber.java.eo.Se;
import io.cucumber.messages.types.Ci;
import org.example.AbstractLayer.AbstractLayer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class RegistrationPage extends AbstractLayer {


    WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        super(driver);
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }


    @FindBy(xpath ="//*[@data-qa='signup-name']")
    WebElement Name;

    @FindBy(xpath = "//*[@data-qa='signup-email']")
    WebElement Email;

    @FindBy(xpath = "//*[@id=\"form\"]/div/div/div[3]/div/form/button")
    WebElement SignupButton;

    @FindBy(id = "id_gender1")
    WebElement checkbox;

    @FindBy(id = "password")
    WebElement Password;

    @FindBy(id = "days")
    WebElement Days;

    @FindBy(id = "months")
    WebElement Months;

    @FindBy(id = "years")
    WebElement Years;

    @FindBy(id = "first_name")
    WebElement FirstName;

    @FindBy(id = "last_name")
    WebElement LastName;

    @FindBy(id = "address1")
    WebElement Address;

    @FindBy(id = "company")
    WebElement Company;

    @FindBy(id = "country")
    WebElement Country;

    @FindBy(id = "state")
    WebElement State;

    @FindBy(id = "city")
    WebElement City;

    @FindBy(id = "zipcode")
    WebElement ZipCode;

    @FindBy(id = "mobile_number")
    WebElement MobileNumber;

    @FindBy(xpath = "//*[@data-qa='create-account']")
    WebElement CreateAccount;

    @FindBy(xpath = "//*[@data-qa='continue-button']")
    WebElement ContinueButton;

    @FindBy(xpath = "//*[@data-qa='account-created']")
    WebElement RegistrationMessage;


    public void RegisterApplication(String name,String email,String password,String days,String months,String years,String firstName,
                                    String lastName,String address,String country,String state,String city,String zipcode,String mobilenumber) {
        Name.sendKeys(name);
        Email.sendKeys(email);
        SignupButton.click();
        checkbox.click();
        FirstName.sendKeys(firstName);
        LastName.sendKeys(lastName);
        Password.sendKeys(password);
        Address.sendKeys(address);
        selectFromDropdown(Days,days);
        selectFromDropdown(Months,months);
        selectFromDropdown(Years,years);
        selectFromDropdown(Country,country);
        State.sendKeys(state);
        City.sendKeys(city);
        ZipCode.sendKeys(zipcode);
        MobileNumber.sendKeys(mobilenumber);
        Click(CreateAccount,driver);
    }

    public String RegistrationMessage() {
    return RegistrationMessage.getText();
    }

    public void signOut(){
        ContinueButton.click();
    }






    }

