package org.example.PageObject;

import org.example.AbstractLayer.AbstractLayer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LandingPage extends AbstractLayer {
    WebDriver driver;

    public LandingPage(WebDriver driver){
        super(driver);
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void goTo(){
        driver.get("https://automationexercise.com/");

    }
}
