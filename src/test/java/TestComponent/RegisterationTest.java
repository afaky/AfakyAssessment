package TestComponent;

import org.example.PageObject.LandingPage;
import org.example.PageObject.RegistrationPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class RegisterationTest extends BaseTest{

    public RegistrationPage registrationPage = new RegistrationPage(driver);

    @Test(dataProvider = "getData")
    public void RegisterAccount(String name,String email,String password,String days,String months,String years,String firstName,String lastName,String address,String country,
                                String state,String city ,String zipcode,String mobilenumber){

        RegistrationPage registrationPage = landingPage.goToRegistrationPage();
        registrationPage.RegisterApplication( name, email, password, days, months, years, firstName, lastName, address, country,
                 state, city , zipcode, mobilenumber);
        String confirmMessage = registrationPage.RegistrationMessage();
        Assert.assertTrue(confirmMessage.equalsIgnoreCase("Account Created!"));
        registrationPage.signOut();

    }
    @DataProvider
    public Object[][] getData()
	  {

	    return new Object[][]  {{"Ahmed","Iamking@000","qwer123","12","8","1995","Ahmed","Alaa","204","Canada","Maadi","Degla","11511","01111356536"} };

	  }

}
