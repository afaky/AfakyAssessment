# Afaky Assessment

The Assessment is divided into 4 parts

1. Amazon searchbox Test Cases.
2. Delivery Vehicle use case.
3. Go Bus App bug report.
4. Automated Use case for registration page using Selenium/Java & Testng.


## Getting Started

### Content

1. First action point can be reviewed by Opening Amazon TCs document.
2. Second action point can be reviewed by opening Delivery Vehicle document.
3. Third action point can be reviewed by opening Go Bus document.
4. Fourth action point can be reviewed by pulling the project on any IDE using JAVA library with    
   version >=17 and pulling the project on your local machine.

* You can run the testng.xml to run the defined scenario.

### Installation

# Navigate to the directory where you want to clone the project
cd path/to/your/directory

# Clone the GitLab repository
git clone https://gitlab.com/afaky/AfakyAssessment.git



